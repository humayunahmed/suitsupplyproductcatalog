﻿namespace ProductCatalog.Repository.Models
{
    using System;

    public class Product
    {
        public DateTime LastUpdated { get; set; }

        public string Name { get; set; }

        public string Photo { get; set; }

        public decimal Price { get; set; }

        public Guid ProductGuid { get; set; }

        public long ProductId { get; set; }
    }
}