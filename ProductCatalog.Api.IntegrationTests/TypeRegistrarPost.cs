﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeRegistrarPost.cs" company="SS">
//   Copyright © SS. All rights reserved.
// </copyright>
// <summary>
//   Unity Registrar
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ProductCatalog.Api.IntegrationTests
{
    using Infrastructure.Interception.Contract;
    using Infrastructure.Repository.Contracts;
    using Infrastructure.Testing.Repository;

    using ProductCatalog.Repository;

    /// <summary>
    ///     Unity Registrar
    /// </summary>
    public class TypeRegistrarPost : ITypeRegistrarPost
    {
        /// <summary>
        ///     The register.
        /// </summary>
        /// <param name="typeRegistrarService">
        ///     The type registrar service.
        /// </param>
        public void Register(ITypeRegistrarService typeRegistrarService)
        {
            typeRegistrarService.RegisterTypeSingleton<IDbConnectionFactoryCustom<ProductCatalogContext>, DbConnectionFactoryTest<ProductCatalogContext>>();
        }
    }
}