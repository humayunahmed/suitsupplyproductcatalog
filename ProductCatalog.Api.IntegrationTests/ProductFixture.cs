﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Infrastructure.Testing.WebApi;

namespace ProductCatalog.Api.IntegrationTests
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using ProductCatalog.Api.Dtos.Views;

    [TestClass]
    public class ProductFixture : BasicRepositorySecured<TestStartup>
    {
        /// <summary>
        /// The set up.
        /// </summary>
        /// <param name="a">
        /// The a.
        /// </param>
        [ClassInitialize]
        public static void SetUp(TestContext a)
        {
            ClassInitializeInitial();
        }

        /// <summary>
        ///     Tears down.
        /// </summary>
        [ClassCleanup]
        public static void TearDown()
        {
            ClassCleanup();
        }
        [TestMethod]
        public async Task ProductCreated()
        {
            var response = await this.GetAsync<ProductView>($"{ApiPaths.TestGet}");
            Assert.IsNotNull(response);
            /*var response = await HttpClient.GetAsync(ApiPaths.TestGet);
            var result = await response.Content.ReadAsStringAsync();*/
        }
    }
}
