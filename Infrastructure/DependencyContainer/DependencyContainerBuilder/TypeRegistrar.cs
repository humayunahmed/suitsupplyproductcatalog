﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.DependencyContainerBuilder
{
    using Infrastructure.DependencyContainerBuilder.Contract;
    using Infrastructure.Interception.Contract;

    class TypeRegistrar : ITypeRegistrar
    {
        public void Register(ITypeRegistrarService typeRegistrarService)
        {
            typeRegistrarService.RegisterTypeSingleton<IApplicationDependecyResolver, ApplicationDependecyResolver>();
        }
    }
}
