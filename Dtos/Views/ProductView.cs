﻿namespace ProductCatalog.Api.Dtos.Views
{
    using System;

    public class ProductView
    {
        public DateTime LastUpdated { get; set; }

        public string Name { get; set; }

        public string Photo { get; set; }

        public decimal Price { get; set; }

        public Guid ProductGuid { get; set; }

        public long ProductId { get; set; }
    }
}