﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountHistoryConfiguration.cs" company="SS">
//   Copyright © SS. All rights reserved.
// </copyright>
// <summary>
//   The account history configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ProductCatalog.Repository.EtConfiguration
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    using ProductCatalog.Repository.Models;

    /// <summary>
    ///     The account history configuration.
    /// </summary>
    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ProductConfiguration" /> class.
        /// </summary>
        public ProductConfiguration()
        {
            this.Property(x => x.LastUpdated).IsRequired();
            this.Property(x => x.Price).IsRequired().HasPrecision(18, 2);
            this.Property(u => u.ProductGuid)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() { IsUnique = true }));

            this.Property(x => x.Name).IsRequired().HasMaxLength(200);
            this.Property(x => x.Photo).IsRequired().HasMaxLength(1000);


        }
    }
}