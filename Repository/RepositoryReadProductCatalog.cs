﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductCatalog.Repository
{
    using Infrastructure.ReadModel;
    using Infrastructure.Repository.Contracts;

    public class RepositoryReadProductCatalog : ReadOptimizedRepository<ProductCatalogReadContext>, IRepositoryReadProductCatalog
    {
        public RepositoryReadProductCatalog(IDbContextFactory<ProductCatalogReadContext> dbContextProvider): base(dbContextProvider)
        {
            
        }
    }
}
