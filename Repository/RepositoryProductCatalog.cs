﻿using System;
using Infrastructure.ReadModel;

namespace ProductCatalog.Repository
{
    using Infrastructure.Repository.Contracts;

    public class RepositoryProductCatalog : Repository<ProductCatalogContext>, IRepositoryProductCatalog
    {
        public RepositoryProductCatalog(IDbContextFactory<ProductCatalogContext> dbContextProvider): base(dbContextProvider)
        {
            
        }
    }
}
