﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityRegistrar.cs" company="SS">
//   Copyright © SS. All rights reserved.
// </copyright>
// <summary>
//   Unity Registrar
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ProductCatalog.Repository
{
    using Infrastructure.Interception.Contract;
    using Infrastructure.ReadModel;
    using Infrastructure.ReadModel.Conventions;
    using Infrastructure.Repository;
    using Infrastructure.Repository.Contracts;

    /// <summary>
    ///     Unity Registrar
    /// </summary>
    public class TypeRegistrar : ITypeRegistrar
    {
        #region Public Methods and Operators

        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="typeRegistrarService">
        /// The type registrar service.
        /// </param>
        public void Register(ITypeRegistrarService typeRegistrarService)
        {
            /*
             if (!typeof(IApplyCatalogNameConvention).IsAssignableFrom(catalogNameConventionType))
            {
                throw new Exception(
                    "'{0}' must implement interface '{1}'".FormatWith(
                        catalogNameConventionType.FullName, 
                        typeof(IApplyCatalogNameConvention).FullName));
            }
             */
            typeRegistrarService.RegisterTypeUnityOfWork<IApplyCatalogNameConvention, ClassicModelNameConvention>();

            typeRegistrarService.RegisterTypeUnityOfWork<IConnectionStringFactory<ProductCatalogContext>, ConnectionStringFactory<ProductCatalogContext>>();
            typeRegistrarService.RegisterTypeUnityOfWork<IDbConnectionFactoryCustom<ProductCatalogContext>, DbConnectionFactory<ProductCatalogContext>>();
            typeRegistrarService.RegisterTypeUnityOfWork<IDbContextFactory<ProductCatalogContext>, ReadModelContextFactory<ProductCatalogContext>>();

            typeRegistrarService.RegisterTypeUnityOfWork<IConnectionStringFactory<ProductCatalogReadContext>, ConnectionStringFactory<ProductCatalogReadContext>>();
            typeRegistrarService.RegisterTypeUnityOfWork<IDbConnectionFactoryCustom<ProductCatalogReadContext>, DbConnectionFactory<ProductCatalogReadContext>>();
            typeRegistrarService.RegisterTypeUnityOfWork<IDbContextFactory<ProductCatalogReadContext>, ReadModelContextFactory<ProductCatalogReadContext>>();

          
            typeRegistrarService.RegisterType<ITestInfo, TestInfo>();

            typeRegistrarService.RegisterTypeUnityOfWork<IRepositoryReadProductCatalog, RepositoryReadProductCatalog>();
            typeRegistrarService.RegisterTypeUnityOfWork<IRepositoryProductCatalog, RepositoryProductCatalog>();
        }

        #endregion
    }
}