﻿namespace ProductCatalog.Repository
{
    using System.Data.Common;

    public class ProductCatalogReadContext : ProductCatalogContext
    {
        public ProductCatalogReadContext()
        {
        }

        public ProductCatalogReadContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public ProductCatalogReadContext(DbConnection existingConnection)
            : base(existingConnection)
        {
            // TODO: Need to analyze the performance of this audit mechanism
            // this.Audit();
        }
    }
}