// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Configuration.cs" company="SS">
//   Copyright � SS. All rights reserved.
// </copyright>
// <summary>
//   The configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ProductCatalog.Repository
{
    using System.Data.Entity.Migrations;

    /// <summary>
    ///     The configuration.
    /// </summary>
    public class Configuration : DbMigrationsConfiguration<ProductCatalogContext>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Configuration" /> class.
        /// </summary>
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
            this.AutomaticMigrationDataLossAllowed = false;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The seed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        protected override void Seed(ProductCatalogContext context)
        {
            //DefaultData.Seed(context);
        }

        #endregion
    }
}