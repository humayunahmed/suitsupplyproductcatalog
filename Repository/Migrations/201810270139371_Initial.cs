namespace ProductCatalog.Repository
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ProductId = c.Long(nullable: false, identity: true),
                        LastUpdated = c.DateTime(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        Photo = c.String(nullable: false, maxLength: 1000),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .Index(t => t.ProductGuid, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Product", new[] { "ProductGuid" });
            DropTable("dbo.Product");
        }
    }
}
