﻿namespace ProductCatalog.Repository
{
    using System.Data.Common;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    using Infrastructure.Repository;

    using ProductCatalog.Repository.EtConfiguration;
    using ProductCatalog.Repository.Models;

    public class ProductCatalogContext : ContextBase
    {
        public ProductCatalogContext()
        {
            
        }

        public ProductCatalogContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            
        }

        public ProductCatalogContext(DbConnection existingConnection)
            : base(existingConnection)
        {
            // TODO: Need to analyze the performance of this audit mechanism
           // this.Audit();
        }

        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// The on model creating.
        /// </summary>
        /// <param name="modelBuilder">
        /// The model builder.
        /// </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new ProductConfiguration());

            base.OnModelCreating(modelBuilder);
        }


    }
}