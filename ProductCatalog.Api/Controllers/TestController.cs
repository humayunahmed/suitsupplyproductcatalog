﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProductCatalog.Api.Controllers
{
    using System.Data.Entity;
    using System.Threading.Tasks;

    using ProductCatalog.Api.Dtos.Views;
    using ProductCatalog.Repository;
    using ProductCatalog.Repository.Models;

    public class TestController : ApiController
    {
        private readonly IRepositoryProductCatalog repositoryProductCatalog;

        public TestController(IRepositoryProductCatalog repositoryProductCatalog)
        {
            this.repositoryProductCatalog = repositoryProductCatalog;
        }
        [HttpGet]
        [Route("api/Test/Get")]
        public async Task<ProductView> Get()
        {
            var test=this.repositoryProductCatalog.Get<Product>();
            var ss = await test.ToListAsync();

            ProductView productView=new ProductView();
            productView.ProductId = 1;
            return productView;
        }
    }
}
